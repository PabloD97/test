while True:
   print("Opcion:")
   print("Ingrese 'sumar' para agregar dos numeros")
   print("Ingrese 'restar' para agregar dos numeros")
   print("Ingrese 'multiplicar' para multiplicar dos numeros")
   print("Ingrese 'dividir' para divir dos numeros")
   print("Ingrese 'salir' para salir del programa")
   usuario_ingreso = input(": ")

   if usuario_ingreso == "salir":
      break
   elif usuario_ingreso == "sumar":
      number1=float(input("Ingrese un numero:"))
      number2=float(input("Ingrese otro numero:"))
      result=str(number1 + number2)
      print("El resultado es " + result)

   elif usuario_ingreso == "restar":
      number1=float(input("Ingrese un numero:"))
      number2=float(input("Ingrese otro numero:"))
      result=str(number1 - number2)
      print("El resultado es " + result)

   elif usuario_ingreso == "multiplicar":
      number1=float(input("Ingrese un numero:"))
      number2=float(input("Ingrese otro numero:"))
      result=str(number1 * number2)
      print("El resultado es " + result)

   elif usuario_ingreso == "dividir":
      number1=float(input("Ingrese un numero:"))
      number2=float(input("Ingrese otro numero:"))
      result=str(number1 / number2)
      print("El resultado es " + result)

   else:
      print("Entrada incorrecta")
